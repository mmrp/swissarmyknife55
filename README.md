
# **Install** #

version: 0.5a

```
#!bash
composer require mmrp/swissarmyknife55 dev-master
```

aggiungere in config/app.php


```
#!php
Mmrp\Swissarmyknife\SwissArmyKnifeProvider::class,
```

eseguire****


```
#!bash
php artisan vendor:publish  --force 
```

aggiungere in Http/Kernel.php nelle route middleware la seguente linea:

```
#!php
'permissions'=> \App\Http\Middleware\Permissions::class
```

eseguire

```
#!bash
composer dump-autoload;

php artisan migrate
php artisan db:seed
```