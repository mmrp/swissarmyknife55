<?php
/**
 * Parse a spreadsheet and return a Laravel Collection. Each Collection's element has as many columns as there are
 * header columns. If $toRow is specified, Collection has ($toRow-1)-$fromRow elements; else Collection has as many
 * elements as there are rows from the row $fromRow to the end of file.
 * Each element belonging to the returned collection is an array having $headers element as "keys" and spreadsheet's
 * column values for a given row as "values". If the spreadsheet has not an header, this information MUST be passed
 * as a parameter and MUST be known beforehand. If the spreadsheet has not an header, fake header columns will be
 * created. Field delimiter parameter ($delimiter) MUST be specified if the spreadsheet is a CSV and must be known
 * beforehand. Passing an unsupported filetype will produce errors. Allowed filetypes are: CSV, ODS, XLSX. No XLS
 * support. Rows counting a different number of columns than the $header will be filled with empty strings.
 *
 * @param $filepath :spreadsheet file full path;
 * @param $fromRow :return spreadsheet rows starting from this row number;
 * @param NULL $toRow :if this parameter is NULL, the spreadsheet will be read from $fromRow to the ending row, else
 *        it will be read until $toRow is reached;
 * @param $hasHeader :this parameter marks if the current spreadsheet has an header or not. if $hasHeader ==1
 *        current spreadsheet has an header, otherwise if $hasHeader==0 it has not an header. It must be known
 *        beforehand;
 * @param $delimiter :used when the spreadsheet is a CSV. It is the field delimiter of the CSV. It must be known
 *        beforehand;
 *
 * @return Collection :the Laravel Collection to be returned. If $toRow is specified, collection will contain
 *         all rows starting from spreadsheet's line $fromRow and ending spreadsheet's $toRow-1.
 */

namespace Mmrp\Swissarmyknife\Lib;

use Box\Spout\Common\Type;
use Box\Spout\Reader\ReaderFactory;
use Box\Spout\Writer\WriterFactory;
use Illuminate\Support\Collection;

trait SpoutTrait
{
    public function parseSpreadSheet($filepath, $fromRow, $toRow=NULL, $hasHeader, $delimiter)
    {
        if($hasHeader){
            $fromRow++;
            $toRow = ($toRow == 'all') ? NULL : $toRow + 1;
        } else {
            $toRow = ($toRow == 'all') ? NULL : $toRow;
        }

        $collection = new Collection();
        $reader = $this->getReader($filepath, $delimiter);
        $header = $this->getHeader($reader, $hasHeader);

        $currentRow=0;
        try {
            foreach ($reader->getSheetIterator() as $sheet) {
                foreach ($sheet->getRowIterator() as $row) {

                    if ($currentRow >= $fromRow && (($toRow == NULL) ? TRUE : $currentRow < $toRow) && $currentRow > -1) {
                        $row =
                            array_map(
                                function ($value) {
                                    if ($value instanceof \DateTime) {
                                        return $value->format(\DateTime::ATOM);
                                    }else{
                                        return $value;
                                    }
                                },
                                $row
                            );

                        // if last column inside this row are empty, the fill the remaining columns with an empty string
                        if(count($row) < count($header)){
                            while(count($row) < count($header)){
                                array_push($row,"");
                            }
                        }

                        if(count($row)>count($header)){
                            throw new \Exception("More columns in row than in header!");
                        }

                        $collectionElement = array_combine($header, $row);
                        $collectionObject = $this->arrayToObject($collectionElement);
                        $collection->push($collectionObject);
                    }
                    $currentRow++;
                }
            }
        }catch(\Exception $e) {
            dd($e->getMessage());
        }

        return collect($collection);
    }

    /**
     * @param $array
     * @return bool|\stdClass
     */
    public  function arrayToObject($array) {
        if (!is_array($array)) {
            return $array;
        }

        $object = new \stdClass();
        if (is_array($array) && count($array) > 0) {
            foreach ($array as $name=>$value) {
                $name = strtolower(trim($name));
                if (!empty($name)) {
                    $object->$name = $this->arrayToObject($value);
                }
            }
            return $object;
        }
        else {
            return FALSE;
        }
    }

    /**
     * Obtain a spreadsheet reader suitable for the given filetype.
     *
     * @param $filepath :full path of the file
     * @param $delimiter :field delimiter (used only for CSV)
     * @return \Box\Spout\Reader\ReaderInterface|NULL :returns a Reader object suitable for the filetype
     * @throws \Box\Spout\Common\Exception\UnsupportedTypeException
     */
    public function getReader($filepath,$delimiter)
    {
        $fileType = pathinfo($filepath,PATHINFO_EXTENSION);
        $reader = NULL;

        switch($fileType){
            case 'xlsx':
                $reader = ReaderFactory::create(Type::XLSX);
                break;
            case 'csv':
                $reader = ReaderFactory::create(Type::CSV);
                $reader->setFieldDelimiter($delimiter);
                break;
            case 'ods':
                $reader = ReaderFactory::create(Type::ODS);
                break;
            default:
                $reader = ReaderFactory::create($fileType); // Unsupported filetype
                break;
        }
        $reader->open($filepath);
        return $reader;
    }

    /**
     * Obtain an header for the collection to be returned.
     *
     * @param $reader :the spreadsheet reader suitable for the filetype
     * @param $hasHeader :it may be 0 or 1. If 0 => no header columns for the file, if 1 => file has header columns
     * @return array :it contains an array of strings with the header columns.
     */
    public function getHeader($reader, $hasHeader)
    {
        $header = array();
        $rowCounter = 0;

        foreach ($reader->getSheetIterator() as $sheet) {
            foreach ($sheet->getRowIterator() as $row) {
                if($rowCounter > 0){
                    break;
                }
                if($hasHeader == 0){
                    $columns = count($row);
                    for($i = 0; $i < $columns; $i++){
                        array_push($header,'header_'.$i);
                    }
                }else {
                    $header = $row;
                }
                $rowCounter++;
            }
        }

        return $header;
    }

    /**
     * Given an array or a Laravel Collection object, obtain a spreadsheet for the collection/array
     *
     * @param $collection :the collection/array to write
     * @param $filepath :the spreadsheet file's name
     * @param $spreadsheetFormat :the spreadsheet format; allowed formats are: "xlsx","csv","ods"
     * @param null $csvFieldDelimiter :when csv spreadsheet is selected, use this column delimiter
     * @throws \Exception :if the parameter $collection is neither a Laravel Collection nor an array
     * throw a new Exception.
     */
    public function laravelCollectionToSpreadsheetFile($collection, $filepath, $spreadsheetFormat,$csvFieldDelimiter=null){

        $writer = $this->getWriter($spreadsheetFormat, $csvFieldDelimiter);
        $writer->openToFile($filepath);

        if($collection instanceof Collection) {
            $header = $this->retrieveHeaderFromCollection($collection);

            $rowCounter = 0;
            foreach ($collection as $collectionElement) {
                if ($rowCounter == 0) {
                    $writer->addRow(array_values($header));
                } else {
                    $arr = (array)$collectionElement;
                    $writer->addRow(array_values($arr));
                }
                $rowCounter++;
            }
        } else if ( is_array($collection) ){
            $header = $this->retrieveHeaderFromArray($collection);
            $writer->addRow(array_values($header));
            foreach ($collection as $collectionElement) {
                $arr = $collectionElement;
                $writer->addRow(array_values($arr));
            }
        } else {
            $writer->close();
            throw new \Exception("Collection parameter is not a Collection nor an array.");
        }
        $writer->close();
    }

    /**
     * Given a Laravel Collection, obtain an header for the spreadsheet file from the first element.
     * @param Collection $collection :the collection from which the header is retrieved.
     * @return array :an array contaning the header for the collection.
     */
    public function retrieveHeaderFromCollection(Collection $collection){
        $firstElement = $collection->first();
        $vars =  array_keys(get_object_vars($firstElement));
        return $vars;
    }


    /**
     * Given an array, obtain an header for the spreadsheet file from the first array element.
     * @param $array :the array from which the header is retrieved.
     * @return array :an array containing the header for the row's entries.
     */
    public function retrieveHeaderFromArray($array){
        $firstElement = $array[0];
        $header = array_keys($firstElement);
        return $header;
    }

    /**
     * Obtain a spreadsheet writer suitable for the given filetype.
     *
     * @param $spreadsheetFormat :the format of the spreadsheet to be written; allowed formats are
     * csv, ods, xlsx. If the spreadsheet format is unsupported, then throw an exception.
     * @param null $fieldDelimiter :the column delimiter to be used when writing to a csv file
     * @return \Box\Spout\Writer\WriterInterface|null :the writer to be used.
     * @throws \Box\Spout\Common\Exception\UnsupportedTypeException :an exception will be thrown
     * when spreadsheet format is not allowed.
     */
    public function getWriter($spreadsheetFormat,$fieldDelimiter=null){
        $writer = null;
        switch($spreadsheetFormat){
            case 'xlsx':
                $writer = WriterFactory::create(Type::XLSX);
                break;
            case 'csv':
                $writer = WriterFactory::create(Type::CSV);
                $writer->setFieldDelimiter($fieldDelimiter);
                break;
            case 'ods':
                $writer = WriterFactory::create(Type::ODS);
                break;
            default:
                // this will throw an error...
                $writer = WriterFactory::create("UNKNOWN");
                break;
        }
        return $writer;
    }
}