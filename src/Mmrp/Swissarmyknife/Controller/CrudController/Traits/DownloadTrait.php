<?php
/**
 * Created by PhpStorm.
 * User: MatteoMeloni
 * Date: 13/06/17
 * Time: 12:21
 */

namespace Mmrp\Swissarmyknife\Controller\Traits;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Mmrp\Swissarmyknife\Lib\Jobs\ExportData;
use Mmrp\Swissarmyknife\Lib\Log;


trait DownloadTrait
{
    /**
     * Used to enable/disable download() method
     * @var bool
     */
    protected $download = TRUE;

    /**
     * Used to enable/disable export() method
     */
    protected $export = TRUE;
    protected $toJobExport = NULL;

    /**
     * Donwload request file
     * @param Request $request
     * @param null $path
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function download(Request $request, $path = NULL)
    {
        if(!$this->download){
            abort(501);
        }

        try{
            Log::info(new \Exception('download', 200), $request,
                [
                    'action' => 'download',
                    'resource' => $this->resource,
                ]
            );

            $this->beforeDownload($request, $path);

            return response()->download(storage_path(base64_decode($path)));
        }
        catch (\Exception $e){
            Log::info(new \Exception('download', 200), $request,
                [
                    'action' => 'download',
                    'resource' => $this->resource,
                ]
            );
        }
    }

    /**
     * Called by child class, execute operation before downloaded
     * @param Request $request
     * @param null $path
     */
    protected function beforeDownload(Request $request, $path = NULL)
    {

    }

    /**
     * @param Request $request
     * @param $type
     * @return array
     * @throws \Exception
     */
    public function export(Request $request, $type)
    {
        if(!$this->export){
            abort(501);
        }

        if(!in_array($type, [ 'xlsx', 'csv' ])){
            throw new \Exception('Invalid Format. Use xlsx or csv',500);
        }

        $this->toJobExport = new \stdClass();
        $this->toJobExport->model = $this->model;
        $this->toJobExport->input = $request->input();

        $this->toJobExport->delimiter = ($type == 'csv') ? ',' : NULL;

        $this->prepareExport($request, $type);

        dispatch(new ExportData($this->toJobExport));

        return [
            'code' => 200,
            'message' => trans('messages.export.prepare')
        ];
    }

    /**
     * Called by child class, prepare @index environment
     * @param Request $request
     * @param $type
     */
    public function prepareExport(Request $request, $type)
    {
        $this->toJobExport->subject = '';
        $this->toJobExport->to = '';
        $this->toJobExport->filename = '';
        $this->toJobExport->download_link = '';
    }

    /**
     * @param Request $request
     * @param $filename
     * @return mixed
     */
    public function downloadExport(Request $request, $filename)
    {
        if(!$this->download){
            abort(501);
        }

        try {
            Log::info(new \Exception('download', 200), $request,
                [
                    'action' => 'download',
                    'resource' => $this->resource,
                ]
            );
            if (env('UPLOAD_TO_S3') == 'true') {
                $mime = Storage::disk('s3')->mimeType('/export/data/' . $filename);
                $download = Storage::disk('s3')->get('/export/data/' . $filename);

                return response($download)
                    ->header('Content-type', $mime)
                    ->header('Content-Disposition', 'attachment; filename=' . $filename);
            } else {
                return response()->download(env('FILE_PATH') . 'export/data/' . $filename);
            }
        }
        catch (\Exception $e){
            Log::info(new \Exception('download', 200), $request,
                [
                    'action' => 'download',
                    'resource' => $this->resource,
                ]
            );
        }
    }
}