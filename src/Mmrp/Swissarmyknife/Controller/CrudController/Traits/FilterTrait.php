<?php
/**
 * Created by PhpStorm.
 * User: MatteoMeloni
 * Date: 14/11/17
 * Time: 14:21
 */

namespace Mmrp\Swissarmyknife\Controller\Traits;

use Illuminate\Http\Request;

trait FilterTrait
{
    protected function filterAndOrder(Request $request, $resource)
    {
        $relation_filter = [];

        if (count($request->input())) {
            foreach ($request->input() as $filter => $value) {
                if ($this->related and $value and preg_match('/^sr_/',$filter)) {
                    foreach ($this->related as $relation) {
                        if(preg_match('/sr_' . $relation . '/', $filter)){
                            $field = str_replace('sr_' . $relation . '_','', $filter);
                            $relation_filter[$relation][$field] = $value ;
                        }
                    }
                }

                if ($value and preg_match('/^s_/',$filter)) {
                    $resource = $resource->where(substr($filter,2), 'like', '%' . str_replace(' ', '%', addslashes($value)) . '%');
                }

                if ($value and preg_match('/^o_/',$filter)) {
                    $resource = $resource->orderBy(substr($filter,2), $value );
                }
            }
        }

        if(!empty($relation_filter)){
            foreach ($relation_filter as $relation => $filters){
                $resource = $resource->whereHas($relation, function ($query) use ($filters){
                    foreach ($filters as $field => $value) {
                        $query->where($field, 'like', '%' . str_replace(' ', '%', addslashes($value)) . '%');
                    }
                });
            }
        }

        return $resource;
    }

    protected function addFilter(Request $request, $resource)
    {
        return $resource;
    }
}