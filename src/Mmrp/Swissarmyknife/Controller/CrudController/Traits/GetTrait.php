<?php
/**
 * Created by PhpStorm.
 * User: MatteoMeloni
 * Date: 13/06/17
 * Time: 11:59
 */

namespace Mmrp\Swissarmyknife\Controller\Traits;

use Illuminate\Http\Request;
use Mmrp\Swissarmyknife\Lib\Log;

trait GetTrait
{

    /**
     * Used to enable/disable get() method
     * @var bool
     */
    protected $get = TRUE;

    /**
     * Return View that contains a specified row
     * @param Request $request
     * @param $id
     * @return View
     */
    public function get(Request $request, $id)
    {
        if(!$this->get){
            abort(501);
        }

        $id = $request->route()->parameter('id');

        try {
            if(!is_null($this->related)) {
                $this->model = $this->model->with($this->related);
            }

            $this->model = $this->model->findOrFail($id);

            $this->prepareGet($request, $id);

            Log::info(new \Exception('get',200), $request, [
                    'action' => 'get',
                    'resource' => $this->resource,
                    'resource_id' => $id
                ]
            );

            return $this->response($request, [
                'data' => $this->model,
                'additional_data' =>$this->additional_data,
                'ui' => [
                    'action' => $this->action,
                    'parameters' => $this->parameters,
                    'resource' => $this->resource,
                    'title' => $this->title,
                    'fields' => $this->fields,
                    'translate_fields' => $this->translate_fields,
                    'fields_types' => $this->fields_types,
                    'breadcrumbs' =>$this->breadcrumbs,
                    'available_action' => $this->prepareAction(),
                    'custom_actions' => $this->custom_action,
                ]
            ]);
        }
        catch (\Exception $e) {
            Log::info($e, $request, [
                    'action' => 'get',
                    'resource' => $this->resource,
                    'resource_id' => $id
                ]
            );
        }
    }

    /**
     * Called by child class, prepare @get environment
     * @param Request $request
     * @param $id
     */
    protected function prepareGet(Request $request, $id)
    {

    }
}